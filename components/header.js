import React from 'react' ;

import {View, Text, StyleSheet,Image} from 'react-native';

const Header = props => {
    return(
        <View style={styles.header}> 
            
            <Text style={styles.headerTitle}>wdw dw dwwd </Text>
        </View>
    );
};

const styles = StyleSheet.create({
    header:{
       width: '100%',
       height: 50,
       backgroundColor:'#191970',
       alignItems:'center',
       justifyContent:'center',
       margin: 0,
       padding: 0
    },
    headerTitle:{
        color: 'white',
        fontSize: 20
    }
});

export default Header;