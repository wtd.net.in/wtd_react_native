import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';

const Next = props => {
  return (
        <TouchableOpacity style={styles.button} onPress={props.onPress} activeOpacity={0.5}>
          <Text style={styles.buttonText}>{props.children}</Text>
        </TouchableOpacity>

  );
};

const styles = StyleSheet.create({
  button: {
    backgroundColor: 'white',
    height: 40,
    width: "25%",
    marginVertical: 10,
    borderRadius: 5,
    alignItems: 'center', 
    justifyContent: 'space-around',
    paddingHorizontal: 5,
    borderColor: '#e3dddc',
    borderWidth: 1
  },
  buttonText: {
    color: 'blue',
    fontSize: 12
  }
});

export default Next;
