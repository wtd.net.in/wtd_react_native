import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';

const AddDesc = props => {
  return (
    <TouchableOpacity style={{...styles.button, ...props.style}} onPress={props.onPress} activeOpacity={0.5}>
      <Text style={styles.buttonText}>{props.children}</Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  button: {
    backgroundColor: '#f5f0f0',
    height: 30,
    marginTop: 10,
    borderRadius: 500,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around'
  },
  buttonText: {
    color: 'black',
    fontSize: 12
  }
});

export default AddDesc;
