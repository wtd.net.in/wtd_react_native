import React from "react";
import {
  View,
  Text,
  TextInput,
  StyleSheet,
  SafeAreaView,
  StatusBar,
} from "react-native";

import { AntDesign } from "@expo/vector-icons";

const InputField = (props) => {
  const clr = props.color ? props.color : "#444";
  const [value, onChangeText] = React.useState();
  const [isFocused, changeFocus] = React.useState(false);
  return (
    <View style={styles.container}>
      <AntDesign style={styles.icon} name="search1" size={24} color="#111" />
      <TextInput
        style={styles.inputField}
        underlineColorAndroid={isFocused ? clr : "#ddd"}
        selectionColor={clr}
        placeholder={props.placeholder}
        onChangeText={(text) => onChangeText(text)}
        onFocus={() => changeFocus(true)}
        onBlur={() => changeFocus(false)}
        value={value}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    width: "100%",
    padding: 5,
  },
  inputField: {
    height: 40,
    borderWidth: 0,
    flexGrow: 1,
    paddingLeft: 6,
    paddingBottom: 5,
    fontSize: 16,
  },
  icon: {
    position: "relative",
    top: 7,
  },
});

export default InputField;
