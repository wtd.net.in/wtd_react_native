import React from 'react';
import { StyleSheet, View, Text, TouchableOpacity } from 'react-native';

const wtdScreen = props => {
    return (
        <View style={styles.ButtonView}>
            <TouchableOpacity
                activeOpacity={0.6}
                onPress={}>
                <View style={styles.cbutton}>
                    <Text style={styles.ButtonText}>+</Text>
                </View>
            </TouchableOpacity>
        </View>
    );
};

const styles = StyleSheet.create({
    cbutton: {
        padding: 10,
        backgroundColor: Platform.OS === 'android' ? Colors.primary : Colors.primary,
        height: 50,
        width: 50,
        borderRadius: 25,
        marginBottom: 60,
        marginRight: 50,
        alignItems: 'center',
        justifyContent: 'center',
    },
    ButtonView: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'flex-end',
        justifyContent: 'flex-end'
    },
    ButtonText: {
        color: 'white',
        fontSize: 30
    }
});

export default wtdScreen;