import { AntDesign } from '@expo/vector-icons';
import * as React from 'react';

import Colors from '../constants/Colors';

export default function NavigatorIcon(props) {
  var clr;
  var size = 25;

  if (props.isAdd) {
    size = 25;
    clr = Colors.addIcon;
  } else {
    if (props.focused) {
      clr = Colors.tabIconSelected;
    } else {
      clr = Colors.tabIconDefault;
    }

  }
  return (
    <AntDesign
      name={props.name}
      size={size}
      color={clr}
    />
  );
}
