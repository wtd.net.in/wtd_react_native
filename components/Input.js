import React, { useState } from 'react';
import { View, TextInput, StyleSheet, TouchableWithoutFeedback, Text } from 'react-native';

const Input = props => {

    return (

        <View style={[
            { ...styles.input, ...props.style }, {
                borderBottomColor: props.selected === true ? 'blue' : 'red'
            }]}>
            <Text>{props.children}</Text>
            <TextInput
                value={props.value}
                defaultValue={props.defaultValue}
                onChangeText={props.onChangeText}
                keyboardType={props.keyboardType} />
        </View>
    );
}

const styles = StyleSheet.create({
    input: {
        backgroundColor: '#f5f0f0',
        marginVertical: 10,
        paddingVertical: 5,
        borderBottomWidth: 1,
        paddingHorizontal: 10,
        borderTopLeftRadius: 5,
        borderTopRightRadius: 5
    }
});

export default Input;

{/* <View style={[
            { ...styles.input, ...props.style }, {
                borderBottomColor: props.selected === true ? 'blue' : 'red'
            }]}></View> */}