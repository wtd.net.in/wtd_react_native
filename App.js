import { AntDesign } from '@expo/vector-icons';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { SplashScreen } from 'expo';
import * as Font from 'expo-font';
import * as React from 'react';
import { Platform, StatusBar, StyleSheet, View } from 'react-native';

import BottomNavigator from "./navigation/BottomNavigator";
import LinkingConfiguration from './navigation/LinkingConfiguration';

const BaseNavigator = createStackNavigator();

export default function App(props) {
  const [isLoadingComplete, setLoadingComplete] = React.useState(false);

  // Load any resources or data that we need prior to rendering the app
  React.useEffect(() => {
    async function loadResourcesAndDataAsync() {
      try {
        SplashScreen.preventAutoHide();

        // Load fonts
        await Font.loadAsync({
          ...AntDesign.font,
          'GorditaBlack': require('./assets/fonts/Gordita/Gordita_Black.otf'),
          'GorditaBold': require('./assets/fonts/Gordita/Gordita_Bold.otf'),
          'GorditaMedium': require('./assets/fonts/Gordita/Gordita_Medium.otf'),
          'Gordita': require('./assets/fonts/Gordita/Gordita_Regular.otf'),
          'GorditaLight': require('./assets/fonts/Gordita/Gordita_Light.otf'),
          'SSProBlack': require('./assets/fonts/SourceSans/SourceSansPro-Black.ttf'),
          'SSProBold': require('./assets/fonts/SourceSans/SourceSansPro-Bold.ttf'),
          'SSProSemiBold': require('./assets/fonts/SourceSans/SourceSansPro-SemiBold.ttf'),
          'SSPro': require('./assets/fonts/SourceSans/SourceSansPro-Regular.ttf'),
          'SSProLight': require('./assets/fonts/SourceSans/SourceSansPro-Light.ttf')
        });
      } catch (e) {
        // We might want to provide this error information to an error reporting service
        console.warn(e);
      } finally {
        setLoadingComplete(true);
        SplashScreen.hide();
      }
    }

    loadResourcesAndDataAsync();
  }, []);

  if (!isLoadingComplete && !props.skipLoadingScreen) {
    return null;
  } else {
    return (
      <View style={styles.container}>
        {Platform.OS === 'ios' && <StatusBar barStyle="dark-content" />}
        <NavigationContainer linking={LinkingConfiguration}>
          <BaseNavigator.Navigator>
            <BaseNavigator.Screen name="Root" component={BottomNavigator}/>
          </BaseNavigator.Navigator>
        </NavigationContainer>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
});
