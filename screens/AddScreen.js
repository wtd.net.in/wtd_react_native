import React, { useState } from 'react';
import {
    Text,
    View,
    Button,
    StyleSheet,
    TouchableOpacity,
    TouchableWithoutFeedback
} from 'react-native';
import Modal from 'react-native-modal';
import DateTimePicker from '@react-native-community/datetimepicker';
import { FontAwesome5 } from '@expo/vector-icons';

import Card from '../components/Card';
import Input from '../components/Input';
import AddDesc from '../components/AddDesc';
import Next from '../components/Next';

const AddScreen = props => {

    const [counter, setCounter] = useState(0);
    const [date, setDate] = useState(new Date(1598051730000));
    const [mode, setMode] = useState('date');
    const [show, setShow] = useState(false);
    const [enteredTask, setEnteredTask] = useState('');
    const [descPressed, setDescPressed] = useState(false);
    const [isModalVisible, setModalVisible] = useState(false);
    const [enteredHours, setEnteredHours] = useState(2);
    const [enteredQuanta, setEnteredQuanta] = useState(1);

    const onChange = (event, selectedDate) => {
        const currentDate = selectedDate || date;
        setShow(Platform.OS === 'ios');
        setDate(currentDate);
    };

    const showMode = currentMode => {
        setShow(true);
        setMode(currentMode);
    };

    const showDatepicker = () => {
        showMode('date');
    };

    const showTimepicker = () => {
        showMode('time');
    };

    const descPressedHandler = () => {
        if (descPressed === true)
            setDescPressed(false);
        else
            setDescPressed(true);
    };

    const toggleModal = () => {
        setModalVisible(!isModalVisible);
    };

    const taskInputHandler = data => {
        setEnteredTask(data);
    };

    const hoursInputHandler = data => {
        setEnteredHours(data);
    };

    const timeQuantaHandler = data => {
        setEnteredQuanta(data);
    };

    const doneHandler = () => {
        if (enteredTask === ' ' || enteredTask === '' || enteredHours <= 0) {
            //console.log('nhi ho rha')
        }
        else {
            setModalVisible(!isModalVisible);
            setEnteredTask(' ');
            setEnteredHours(2);
            setEnteredQuanta(1);
            setDescPressed(false);
        }
    }

    let descOutput;

    if (descPressed) {
        descOutput = (
            <Input style={styles.descInput} selected={true}>
                <Text style={{ fontSize: 10, color: 'blue' }}>Description</Text>
            </Input>
        );
    }

    return (
        <View style={styles.screen}>

            {/* + button */}
            <View style={styles.ButtonView}>
                <TouchableOpacity
                    activeOpacity={0.6}
                    onPress={toggleModal}>
                    <View style={styles.cbutton}>
                        <Text style={styles.ButtonText}>+</Text>
                    </View>
                </TouchableOpacity>
            </View>

            {/* Popup starts */}

            <Modal isVisible={isModalVisible} animationType='fade'>
                {/* Card Component starts */}
                <Card>

                    {/* Welcome text View*/}
                    <Text style={styles.textView}>
                        Got another thrilling task Luna? Add it and
                    <Text style={styles.boldTextView}> just do it!</Text>
                    </Text>

                    {/* Task Name Input View*/}
                    <Input
                        style={styles.taskInput}
                        onChangeText={taskInputHandler}
                        selected={enteredTask === ' ' || enteredTask === '' ? false : true}
                    >
                        <Text style={{ fontSize: 10, color: enteredTask === ' ' || enteredTask === '' ? 'red' : 'blue' }}>Task Name*</Text>
                    </Input>

                    {/* Hours and Minimum Quanta */}
                    <View style={styles.lowerInputView}>
                        <Input
                            style={styles.otherInput}
                            keyboardType='numeric'
                            defaultValue='2'
                            onChangeText={hoursInputHandler}
                            selected={enteredHours <= 0 ? false : true}>
                            <Text style={{ fontSize: 10, color: enteredHours <= 0 ? 'red' : 'blue' }}>Hours of Completion</Text>
                        </Input>
                        <Input
                            style={styles.otherInput}
                            keyboardType='numeric'
                            defaultValue='1'
                            onChangeText={timeQuantaHandler}
                            selected={true}>
                            <Text style={{ fontSize: 10, color: 'blue' }}>Minimum Time Quanta</Text>
                        </Input>
                    </View>

                    {/* Add Desciption */}
                    <View style={styles.descriptionView}>
                        <View style={styles.descriptionButttonView}>
                            <AddDesc onPress={descPressedHandler} style={styles.descriptionButtton}>
                                <FontAwesome5 name="pen" size={12} color="black" />
                                <Text> Add Desciption</Text>
                            </AddDesc>
                        </View>
                        <AddDesc onPress={showDatepicker} style={styles.dateTimeButton}>
                            <FontAwesome5 name="calendar-alt" size={12} color="black" />
                            <Text> {date.getDate()}-{date.getMonth()}-{date.getFullYear()}</Text>
                        </AddDesc>
                        <AddDesc onPress={showTimepicker} style={styles.dateTimeButton}>
                            <FontAwesome5 name="clock" size={12} color="black" />
                            <Text> {date.getHours()}:{date.getMinutes()} IST</Text>
                        </AddDesc>
                    </View>

                    {descOutput}

                    {/* Next and Back Button */}
                    <View style={styles.nextButtonView}>
                        <Next onPress={doneHandler}>
                            <Text>Done</Text>
                        </Next>
                        <Next onPress={toggleModal}>
                            <Text>Back</Text>
                        </Next>
                    </View>

                </Card>
                {/* Card Ends */}
            </Modal >

            {/* Popup ends */}


            {
                show && (
                    <DateTimePicker
                        testID="dateTimePicker"
                        timeZoneOffsetInMinutes={0}
                        value={date}
                        mode={mode}
                        is24Hour={true}
                        display="default"
                        onChange={onChange}
                    />
                )
            }

        </View >
    );

};

const styles = StyleSheet.create({
    screen: {
        flex: 1,
    },
    textView: {
        fontSize: 22,
        fontFamily: 'Gordita',
        marginVertical: 10
    },
    boldTextView: {
        fontSize: 22,
        fontFamily: 'GorditaBold'
    },
    taskInput: {
        width: '100%',
        height: 50
    },
    lowerInputView: {
        flexDirection: 'row',
        width: '100%',
        justifyContent: 'space-between'
    },
    otherInput: {
        width: '49%',
        height: 50
    },
    descInput: {
        height: 90,
        borderBottomColor: 'blue',
        borderBottomWidth: 1,
        paddingHorizontal: 10
    },
    nextButtonView: {
        flexDirection: 'row-reverse',
        justifyContent: 'space-between',
        width: "100%",
        paddingHorizontal: 5
    },
    descriptionView: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: "100%",
    },
    inputText: {

    },
    cbutton: {
        padding: 10,
        backgroundColor: 'blue',
        height: 50,
        width: 50,
        borderRadius: 25,
        marginBottom: 60,
        marginRight: 50,
        alignItems: 'center',
        justifyContent: 'center',
    },
    ButtonView: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'flex-end',
        justifyContent: 'flex-end'
    },
    ButtonText: {
        color: 'white',
        fontSize: 30
    },
    button: {
        width: "50%"
    },
    descriptionButttonView: {
        width: '45%'
    },
    dateTimeButton: {
        width: '25%'
    },
    descriptionButtton: {
        width: '75%'
    }
});

export default AddScreen;