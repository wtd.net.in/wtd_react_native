import React from "react";
import {
  View,
  Text,
  TextInput,
  StyleSheet,
  SafeAreaView,
  StatusBar,
} from "react-native";

import InputField from '../components/InputField';

const SearchScreen = (props) => {
  return (
    <SafeAreaView style={styles.container}>
      {Platform.OS === "android" && (
        <StatusBar
          translucent={true}
          backgroundColor="#ccc"
          barStyle="dark-content"
        />
      )}
      <View style={styles.header}>
        <InputField bgcolor="#ccc" placeholder="Search"/>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
    alignItems: "center",
    alignContent: "center",
  },
  header: {
    backgroundColor: "#fff",
    elevation: 2,
  },
});

export default SearchScreen;
