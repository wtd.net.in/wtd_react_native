import React from "react";
import { View, Text, StyleSheet, Button } from "react-native";

const ProfileScreen = (props) => {
  return (
    <View style={{
        flex: 1,
        borderWidth: 0,
        position: "absolute",
        bottom: 0,
        alignSelf: "center",
      }}>
      <View>
        <View style={styles.button} accessibilityLabel="Press">
            <Text style={styles.buttonText}>As</Text>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
    button: {
        backgroundColor:"#ffffff",
        padding: 20,
        borderRadius: 80,
        width: 80,
        height: 80,
        bottom: 1,
        shadowColor: "#eee",
        shadowOpacity: 10,
        shadowRadius: 0,
        elevation: 0,
        shadowOffset: {
            width: 0,
            height: -1
        }
    },
    buttonText: {
        color:"#000000",
    }
});

export default ProfileScreen;
