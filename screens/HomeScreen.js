import React from "react";

import {
  View,
  SafeAreaView,
  Text,
  StyleSheet,
  Button,
  Image,
  TextInput,
  Platform,
  StatusBar,
} from "react-native";
// import { FloatingAction } from "react-native-floating-action";
const HomeScreen = (props) => {
  return (
    <SafeAreaView style={styles.container}>
      {Platform.OS === "android" && <StatusBar translucent={true} backgroundColor="#ccc" barStyle="dark-content"/>}
      <View style={styles.header}>
        <Text style={styles.title}>
          Let's see what can be done rightnow,dude!!
        </Text>
      </View>

      <View style={styles.inputContainer}>
        <Text style={styles.title}>How much time have you got?</Text>

        <View style={styles.buttonContainer}>
          <Button title="15 mins" />
          <Button title="30 mins" />
          <Button title="1 hour" />
          <Button title="2 hour" />
          <Button title="custom" />
        </View>
      </View>
      <Image style={styles.image} source={require("../assets/wtd_bg.png")} />
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
    alignItems: "center",
    alignContent: "center",
  },
  header: {
      backgroundColor: "#ccc",
      elevation: 2,
      width: "100%",
      paddingTop: 10,
  },
  title: {
    textAlign: "center",
    fontSize: 20,
    marginVertical: 10,
    color: "#00008b",
    fontFamily: "GorditaMedium",
  },

  inputContainer: {
    padding: 10,
    width: 300,
    maxWidth: "80%",
    alignItems: "center",
  },

  buttonContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    paddingLeft: 10,
    paddingRight: 10,
  },
  image: {
    width: "50%",
    height: 300,
  },
});

export default HomeScreen;
