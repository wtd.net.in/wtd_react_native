# WtD React Native

React Native App for What to Do?! https://wtd.net.in/

# Running the app
- First **Clone** the app using the below command in your terminal (this command is for main project, you can also use your forked project instead).

	`git clone https://gitlab.com/wtd.net.in/wtd_react_native.git`
- Then `cd` into the **wtd_react_native** directory using
	
	`cd wtd_react_native`
- Install the npm dependecies:
	
	`npm install --save`
- After installing run using:
	
	`npm start`

Now the app will be up and running, to test the app (in Android or ios) follow the below apps.

# Testing the app
Irrespective of the platform (Android, ios or ipadOS) you are using, you need to have the **Expo Client** app installed in your device (it would be better to have a physical device instead of an emulator for better performance). Now follow the below instructions.

### For Android
- Launch the **expo** app in your device and open code scanner.
- Scan the QR code displayed at http://localhost:19002/ (The port number may change from 19002 to something else, you can check it in your terminal where you executed `npm start` )
- The app will open and you can make changes to the code. It will reload whenever you change the code.
### For ios/ipadOS
- After installing the **expo** app, open the **QR code scanner** from the top most status bar (Where you see the battery percentage).
- Scan the QR code displayed at http://localhost:19002/ (The port number may change from 19002 to something else, you can check it in your terminal where you executed `npm start` ).
- The app will open and you can make changes to the code. It will reload whenever you change the code.

# Contributing
You will have to send a **Merge Request** to develop the app. And the code will be checked & get merged by one of us.
1. First Fork the project to your profile by using the **fork** button at the top right (Forking means you are making a copy of the project to work/make changes on it).
2. After forking first **clone** the project using.
	
	`git clone your_project_url`
3. Go inside the project folder using `cd`.
4. Add the main project as **upstream**.
	
	`git remote add upstream https://gitlab.com/wtd.net.in/wtd_react_native.git`
5. Synchronize with the main project:
	
	`git pull upstream master`
6. Make changes to the code you want to do....
7. After finishing, **commit** the code:
	
	`git add -A`
	
	`git commit -m "improved feature..."`
8. Synchronize with the main project:
	
	`git pull upstream master`
8. Push the code to the **origin** (In this case the origin is your forked project, and pushing means you are sending the updated code to gitlab).
	
	`git push origin master`
9. After pushing send a **merge request** to the main project by clicking the Merge Request button on the sidebar.
10. We will merge it to the main project :).
11. Repeat from steps **5** to **10** everytime you want to change the code. (You need to do other steps only one time)


To learn more:
- https://rogerdudler.github.io/git-guide/
- https://product.hubspot.com/blog/git-and-github-tutorial-for-beginners
