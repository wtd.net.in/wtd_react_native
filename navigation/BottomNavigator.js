import React from "react";
import { View, Text, StyleSheet } from "react-native";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";

import NavigatorIcon from "../components/NavigatorIcon";

import HomeScreen from "../screens/HomeScreen";
import ProfileScreen from "../screens/ProfileScreen";
import ZoomOutScreen from "../screens/ZoomOutScreen";
import SearchScreen from "../screens/SearchScreen";
import AddScreen from "../screens/AddScreen";

import Header  from "../components/header";

const BottomTab = createBottomTabNavigator();
const INITIAL_ROUTE_NAME = "Home";

const BottomNavigator = ({ navigation, route }) => {
  navigation.setOptions({ 
    headerTitle: props => <Header {...props} />,
    headerShown: false
  });

  return (
    <BottomTab.Navigator
      tabBarOptions={{ showLabel: false }}
      initialRouteName={INITIAL_ROUTE_NAME}
    >
      <BottomTab.Screen
        name="Home"
        component={HomeScreen}
        options={{
          tabBarIcon: ({ focused }) => (
            <NavigatorIcon focused={focused} name="home" />
          ),
        }}
      />
      <BottomTab.Screen
        name="ZoomOut"
        component={ZoomOutScreen}
        options={{
          tabBarIcon: ({ focused }) => (
            <NavigatorIcon focused={focused} name="appstore-o" />
          ),
        }}
      />
      <BottomTab.Screen
        name="Add"
        component={AddScreen}
        options={{
          tabBarIcon: ({ focused }) => (
            <NavigatorIcon focused={focused} isAdd={true} name="pluscircle" />
          ),
        }}
      />
      <BottomTab.Screen
        name="Search"
        component={SearchScreen}
        options={{
          tabBarIcon: ({ focused }) => (
            <NavigatorIcon focused={focused} name="search1" />
          ),
        }}
      />
      <BottomTab.Screen
        name="Profile"
        component={ProfileScreen}
        options={{
          tabBarIcon: ({ focused }) => (
            <NavigatorIcon focused={focused} name="user" />
          ),
        }}
      />
    </BottomTab.Navigator>
  );
};

export default BottomNavigator;
